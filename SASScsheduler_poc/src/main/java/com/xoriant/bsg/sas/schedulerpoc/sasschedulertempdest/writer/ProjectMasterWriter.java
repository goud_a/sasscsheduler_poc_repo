package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.writer;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterDestination;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.repository.ProjectMasterDestinationRepo;
/**
 * This class for write data into the corresponding database
 * @author goud_a
 *
 */
@Component(value = "projectmasterWriter")
public class ProjectMasterWriter implements ItemWriter<ProjectMasterDestination> {
	private static final Logger LOG=LogManager.getLogger(ProjectMasterWriter.class.getClass());
	@Autowired
	private ProjectMasterDestinationRepo masterDestinationRepo;
	/**
	 * The below method write data into destination table
	 */
	@Override
	public void write(List<? extends ProjectMasterDestination> projectMasterDestinations) throws Exception {
		
		for (ProjectMasterDestination projectMasterDestination : projectMasterDestinations) {
			LOG.info("projectMaster source table persist into destination table :{}",projectMasterDestination);
			masterDestinationRepo.save(projectMasterDestination);
			LOG.debug("projectMaster source table persist into destination table :{}",projectMasterDestination);

		}
	}	

}
