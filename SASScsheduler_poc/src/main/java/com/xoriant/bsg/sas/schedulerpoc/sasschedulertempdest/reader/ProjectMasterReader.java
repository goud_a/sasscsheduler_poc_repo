package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.reader;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.core.RowMapper;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertemp.source.model.ProjectMasterSource;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;

/**
 * This class for read the source table data/record
 * 
 * @author goud_a
 *
 */
public class ProjectMasterReader extends JdbcCursorItemReader<ProjectMasterSource> implements ItemReader<ProjectMasterSource>,ApplicationConstants {

	public ProjectMasterReader(DataSource sourceDataSource) {
		setDataSource(sourceDataSource);
		setSql(SQL_QUERY_FOR_GET_PROJECT_MASTER);
		setFetchSize(100);
		setRowMapper(new projectMasterRowMapper());
	}
	private class projectMasterRowMapper implements RowMapper<ProjectMasterSource>{

		@Override
		public ProjectMasterSource mapRow(ResultSet rs, int rowNum) throws SQLException {
			ProjectMasterSource projectMaster=null;
			projectMaster=new ProjectMasterSource();
			projectMaster.setProjectId(rs.getInt(PROJECT_ID_COLUMN_NAME));
			projectMaster.setProjectNumber(rs.getString(PROJECT_NUMBER_COLUMN_NAME));
			projectMaster.setProjectName(rs.getString(PROJECT_NAME_COLUMN_NAME));
			projectMaster.setProjectType(rs.getString(PROJECT_TYPE_COLUMN_NAME));
			projectMaster.setProjectStartDate(rs.getString(PROJECT_START_DATE_COLUMN_NAME));
			projectMaster.setProjectEndDate(rs.getString(PROJECT_END_DATE_COLUMN_NAME));
			return projectMaster;
		}
		
	}

}
