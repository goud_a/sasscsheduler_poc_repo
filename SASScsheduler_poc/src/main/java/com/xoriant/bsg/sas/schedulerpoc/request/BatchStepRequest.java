package com.xoriant.bsg.sas.schedulerpoc.request;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class BatchStepRequest {
	
	private String stepName;
	private Date startTime;
	private Date endTime;
	private String status;
	private int readCount;
	private int readSkipCount;
	private int writeCount;
	private int writeSkipCount;
	private int commitCount;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getReadCount() {
		return readCount;
	}
	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}
	public int getReadSkipCount() {
		return readSkipCount;
	}
	public void setReadSkipCount(int readSkipCount) {
		this.readSkipCount = readSkipCount;
	}
	public int getWriteCount() {
		return writeCount;
	}
	public void setWriteCount(int writeCount) {
		this.writeCount = writeCount;
	}
	public int getWriteSkipCount() {
		return writeSkipCount;
	}
	public void setWriteSkipCount(int writeSkipCount) {
		this.writeSkipCount = writeSkipCount;
	}
	public int getCommitCount() {
		return commitCount;
	}
	public void setCommitCount(int commitCount) {
		this.commitCount = commitCount;
	}
	@Override
	public String toString() {
		return "BatchStepRequest [stepName=" + stepName + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", status=" + status + ", readCount=" + readCount + ", readSkipCount=" + readSkipCount
				+ ", writeCount=" + writeCount + ", writeSkipCount=" + writeSkipCount + ", commitCount=" + commitCount
				+ "]";
	}
	
	
	
	
}
