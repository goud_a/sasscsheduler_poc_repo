package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.batch.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertemp.source.model.ProjectMasterSource;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterListener;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.reader.ProjectMasterReader;

/**
 * This batch configuration for projectMaster table
 * 
 * @author goud_a
 *
 */
@Configuration
@EnableBatchProcessing
public class SasSchedulerTempDBBatchConfig {
	@Autowired
	@Qualifier("sasSchedulerTempDataSource")
	private DataSource dataSource;

	// 1.Reader
	@Bean
	public ItemReader<ProjectMasterSource> projectMasterReader() {
		return new ProjectMasterReader(dataSource);
	}

	// 2.processor
	@Autowired
	@Qualifier("projectMasterProcessor")
	private ItemProcessor<?, ?> projectMasterProcessor;
	/*
	 * @Bean public ItemProcessor<ProjectMasterSource, ProjectMasterDestination>
	 * projectItemProcessor() { return new ProjectMasterProcessor(); }
	 */
	// 3.Writer
	@Autowired
	@Qualifier("projectmasterWriter")
	private ItemWriter<?> projectmasterWriter;

	/*
	 * @Bean public ItemWriter<ProjectMasterDestination> projectItemWriter() {
	 * return new ProjectMasterWriter(); }
	 */
	// 4.listener
	@Bean
	public JobExecutionListener projectMasterListener() {
		return new ProjectMasterListener();
	}

	// 5.stepBuilderFactory
	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	// 5. step
	@SuppressWarnings("unchecked")
	@Bean
	public Step projectMasterStep() {
		return stepBuilderFactory.get("projectMasterStep").<ProjectMasterSource, ProjectMasterSource>chunk(4)
				.reader(projectMasterReader())
				// .processor(projectItemProcessor())
				.processor(
						(ItemProcessor<? super ProjectMasterSource, ? extends ProjectMasterSource>) projectMasterProcessor)
				.writer((ItemWriter<? super ProjectMasterSource>) projectmasterWriter)
				
				.build();

	}

	// 6. jobBuilderFactory
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	// 7.job
	@Bean(name = "projectMasterJob")
	public Job projectMasterJob() {
		return jobBuilderFactory.get("projectMasterJob").incrementer(new RunIdIncrementer()).listener(projectMasterListener())
				.start(projectMasterStep())
				.listener(projectMasterListener())
				.build();

	}

}
