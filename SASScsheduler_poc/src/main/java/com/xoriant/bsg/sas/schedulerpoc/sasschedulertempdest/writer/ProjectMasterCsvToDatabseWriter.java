package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.writer;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.repository.ProjectMasterCsvRepository;

/**
 * This class write the date into destination database
 * 
 * @author goud_a
 *
 */
@Component
public class ProjectMasterCsvToDatabseWriter implements ItemWriter<ProjectMasterCsv> {

	private static final Logger LOG = LogManager.getLogger(ProjectMasterCsvToDatabseWriter.class.getClass());
	@Autowired
	private ProjectMasterCsvRepository masterRepository;

	/**
	 * This method write the data into database(persistence)
	 */
	@Override
	public void write(List<? extends ProjectMasterCsv> items) throws Exception {

		for (ProjectMasterCsv projectMasterCsv : items) {
			LOG.info("projectMaster csv file persist into database{}", projectMasterCsv);
			masterRepository.save(projectMasterCsv);
			LOG.debug("projectMaster csv file persist into database{}", projectMasterCsv);
		}
	}

}
