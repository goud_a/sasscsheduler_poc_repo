package com.xoriant.bsg.sas.schedulerpoc.db.master.service;

import com.xoriant.bsg.sas.schedulerpoc.request.DestinationDatasourceDetails;

public interface DestinationBatchService {
	public DestinationDatasourceDetails findBySchemName(String schemName);
}
