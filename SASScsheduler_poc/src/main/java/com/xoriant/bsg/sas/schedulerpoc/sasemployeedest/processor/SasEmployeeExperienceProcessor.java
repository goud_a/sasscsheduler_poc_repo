package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.processor;

import java.util.Date;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model.SasEmployeeDestinationExperiance;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeesource.source.model.SasEmployeeSourceExperiance;
@Component("sasEmployeeExperienceProcessor")
public class SasEmployeeExperienceProcessor implements ItemProcessor<SasEmployeeSourceExperiance, SasEmployeeDestinationExperiance>{

	@Override
	public SasEmployeeDestinationExperiance process(SasEmployeeSourceExperiance item) throws Exception {
		// here we can perform operation/filter on source data
		SasEmployeeDestinationExperiance employeeDestinationExperiance=null;
		employeeDestinationExperiance=new SasEmployeeDestinationExperiance();
		BeanUtils.copyProperties(item, employeeDestinationExperiance);
		employeeDestinationExperiance.setStgCreatedOn(new Date());	
		return employeeDestinationExperiance;
	}

}
