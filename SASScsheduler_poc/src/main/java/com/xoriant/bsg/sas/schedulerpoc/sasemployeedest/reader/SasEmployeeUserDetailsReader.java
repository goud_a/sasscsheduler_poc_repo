package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.reader;

import javax.sql.DataSource;

import org.springframework.batch.item.database.JdbcCursorItemReader;

import com.xoriant.bsg.sas.schedulerpoc.db.sasemployee.source.model.UserDetails;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;
/**
 * This class for reading source table data/record
 * @author goud_a
 *
 */
public class SasEmployeeUserDetailsReader extends JdbcCursorItemReader<UserDetails> {

	public SasEmployeeUserDetailsReader(DataSource dataSource) {
		setDataSource(dataSource);
		setFetchSize(ApplicationConstants.FETCH_SIZE);
		setSql(ApplicationConstants.SQL_VIEW_FOR_GET_USER_DETAILS);
		setRowMapper((rs,rowNum)->{
			UserDetails userDetails=null;
			userDetails=new UserDetails();
			userDetails.setFirstName(rs.getString(ApplicationConstants.FIRST_NAME_COLUMN_NAME));
			userDetails.setLastName(rs.getString(ApplicationConstants.LAST_NAME_COLUMN_NAME));
			userDetails.setMobileNumber(rs.getString(ApplicationConstants.MOBILE_NUMBER_COLUMN_NAME));
			userDetails.setStreet(ApplicationConstants.STREET_COLUMN_NAME);
			userDetails.setCity(rs.getString(ApplicationConstants.CITY_COLUMN_NAME));
			userDetails.setState(rs.getString(ApplicationConstants.STATE_COLUMN_NAME));
			userDetails.setCountry(rs.getString(ApplicationConstants.COUNTRY_COLUMN_NAME));
			return userDetails;
		});
	}	

}
