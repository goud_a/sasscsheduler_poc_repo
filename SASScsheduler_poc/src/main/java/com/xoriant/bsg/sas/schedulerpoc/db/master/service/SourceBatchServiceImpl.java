package com.xoriant.bsg.sas.schedulerpoc.db.master.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.SourceBatchDatabase;
import com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository.SourceBatchDatabaseRepository;
import com.xoriant.bsg.sas.schedulerpoc.request.SourceDatabaseDetails;

/**
 * This class for fetch the source database/schema details by using schemmaName
 * 
 * @author goud_a
 *
 */
@Service
public class SourceBatchServiceImpl implements SourceBatchService {
	@Autowired
	private SourceBatchDatabaseRepository repository;
	
	/**
	 * Below method to get schema details by using SchemaName
	 * @param sourceSchemName
	 * @return
	 */
	@Override
	public SourceDatabaseDetails getsouDatabaseDetailsBySchemName(String sourceSchemName) {
		SourceDatabaseDetails sourceDatabaseDetails = null;
		SourceBatchDatabase sourceBatchDatabase = null;
		sourceDatabaseDetails = new SourceDatabaseDetails();
		sourceBatchDatabase = repository.findBySourceSchemName(sourceSchemName);
		BeanUtils.copyProperties(sourceBatchDatabase, sourceDatabaseDetails);
		return sourceDatabaseDetails;

	}
}
