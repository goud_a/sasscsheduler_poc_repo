package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "project_master_csv_stg")
public class ProjectMasterCsv {

	public ProjectMasterCsv() {
		super();
	}

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_id")
	private Integer projectId;
	@Column(name = "project_number")
	private String projectNumber;
	@Column(name = "project_name")
	private String projectName;
	@Column(name = "project_type")
	private String projectType;
	@Column(name = "project_start_date")
	private String projectStartDate;
	@Column(name = "project_end_date")
	private String projectEndDate;

	public ProjectMasterCsv(String projectNumber, String projectName, String projectType, String projectStartDate,
			String projectEndDate) {
		super();
		this.projectNumber = projectNumber;
		this.projectName = projectName;
		this.projectType = projectType;
		this.projectStartDate = projectStartDate;
		this.projectEndDate = projectEndDate;
	}
	
	public ProjectMasterCsv(Integer projectId, String projectNumber, String projectName, String projectType,
			String projectStartDate, String projectEndDate) {
		super();
		this.projectId = projectId;
		this.projectNumber = projectNumber;
		this.projectName = projectName;
		this.projectType = projectType;
		this.projectStartDate = projectStartDate;
		this.projectEndDate = projectEndDate;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public String getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}


}
