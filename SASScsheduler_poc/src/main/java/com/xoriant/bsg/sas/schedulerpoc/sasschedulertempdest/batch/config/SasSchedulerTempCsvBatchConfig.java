package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvJobListener;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvSkipListener;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvSkipPolicy;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvStepExecutionListener;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.processor.ProjectMasterCsvProcesser;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.reader.ProjectMasterCsvReader;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.writer.ProjectMasterCsvToDatabseWriter;

/**
 * This configuration for BatchProcessing and below components are configured
 * for batch processing
 * 
 * @author goud_a
 *
 */
@Configuration
@EnableBatchProcessing
public class SasSchedulerTempCsvBatchConfig {

	// 1.Reader
	/**
	 * This bean configuration component read the data from source location here csv
	 * file is source
	 * 
	 * @return
	 */

	@Bean
	public ItemReader<ProjectMasterCsv> prjectItemReader() {
		FlatFileItemReader<ProjectMasterCsv> fileItemReader = new FlatFileItemReader<ProjectMasterCsv>();
		return new ProjectMasterCsvReader(fileItemReader).projectMasterReader();

	}

	// 2.processor
	/**
	 * {@link ItemProcessor} for after read the data by {@link ItemReader} we can
	 * perform any operation
	 * 
	 * @return
	 */
	@Bean
	public ItemProcessor<ProjectMasterCsv, ProjectMasterCsv> projectMasterCsvProcesser() {
		return new ProjectMasterCsvProcesser();

	}

	// 3.writer
	/**
	 * This configured component meant for write the date into destination
	 * 
	 * @return
	 */
	@Bean
	public ItemWriter<ProjectMasterCsv> projectmasterItemWriter() {
		return new ProjectMasterCsvToDatabseWriter();

	}

	// 4.listener
	//@Bean(name = "projectMasterCsvJobListener")
	@Bean
	public JobExecutionListener projectMasterCsvJobListener() {
		return new ProjectMasterCsvJobListener();
	}

	// stepbuilderFactory
	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	public StepExecutionListener projectMasterCsvStepExecutionListener() {
		return new ProjectMasterCsvStepExecutionListener();
	}
	// 5.step
	@Bean
	public Step projectMasterCsvStep() {
		return stepBuilderFactory.get("projectMasterCsvStep")
				.<ProjectMasterCsv, ProjectMasterCsv>chunk(5)
				.reader(prjectItemReader())
				.processor(projectMasterCsvProcesser())
				.writer(projectmasterItemWriter())
				//.taskExecutor(taskExecutor())
				.faultTolerant().skipPolicy(projectMasterCsvSkipPolicy())	
				.listener(projectMasterCsvSkipListener())				
				.listener(projectMasterCsvStepExecutionListener())
				.build();
	}

	// jobBuilderFactory
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	// 5.job
	@Bean(name="projectCsvMasterJob")
	public Job projectMasterCsvJob() {
		return jobBuilderFactory.get("projectCsvMasterJob")
				.incrementer(new RunIdIncrementer())
				.listener(projectMasterCsvJobListener())				
				.start(projectMasterCsvStep())
				.build();
	}
	/**
	 * This is {@link TaskExecutor} for reading the file in a multi threading environment 
	 * @return
	 */
	@Bean		
	public TaskExecutor taskExecutor() {
		SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
		simpleAsyncTaskExecutor.setConcurrencyLimit(5);
		return simpleAsyncTaskExecutor;
	}
	
	@Bean
	public SkipPolicy projectMasterCsvSkipPolicy() {
		return new ProjectMasterCsvSkipPolicy();
		
	}
	@Bean
	public SkipListener<ProjectMasterCsv, ProjectMasterCsv> projectMasterCsvSkipListener() {
		return new ProjectMasterCsvSkipListener();
		
	}
}
/*
 * //@Bean public ItemReader<ProjectMaster> projectMasterReader() {
 * FlatFileItemReader<ProjectMaster> reader = new
 * FlatFileItemReader<ProjectMaster>(); // 1.Read the file from class paths :
 * location reader.setResource(new ClassPathResource("projectMaster.csv")); //
 * 2.Read the data line reader.setLineMapper(new
 * DefaultLineMapper<ProjectMaster>() { { // 3.split data based on delimiter
 * setLineTokenizer(new DelimitedLineTokenizer() { {
 * setDelimiter(DELIMITER_COMMA); setNames("projectNumber", "projectName",
 * "projectType", "projectStartDate", "projectEndDate"); } });
 * setFieldSetMapper(new BeanWrapperFieldSetMapper<ProjectMaster>() { {
 * setTargetType(ProjectMaster.class); } }); } }); return reader; }
 */

/*
 * //@Bean public ItemWriter<ProjectMaster> projectmasterItemWriter() {
 * JdbcBatchItemWriter<ProjectMaster> jdbcBatchItemWriter = new
 * JdbcBatchItemWriter<ProjectMaster>(); jdbcBatchItemWriter.setSql(
 * "INSERT INTO project_master  (project_number, project_name , project_type , project_start_date , project_end_date ) VALUES (:projectNumber,:projectName, :projectType,:projectStartDate,:projectEndDate)"
 * ); jdbcBatchItemWriter.setDataSource(dataSource); jdbcBatchItemWriter
 * .setItemSqlParameterSourceProvider(new
 * BeanPropertyItemSqlParameterSourceProvider<ProjectMaster>()); return
 * jdbcBatchItemWriter; }
 */