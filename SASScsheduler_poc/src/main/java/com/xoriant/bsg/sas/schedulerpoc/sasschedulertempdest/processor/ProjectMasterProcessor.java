package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertemp.source.model.ProjectMasterSource;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterDestination;
@Component(value = "projectMasterProcessor")
public class ProjectMasterProcessor implements ItemProcessor<ProjectMasterSource, ProjectMasterDestination> {

	@Override
	public ProjectMasterDestination process(ProjectMasterSource projectMasterSource) throws Exception {
		ProjectMasterDestination projectMasterDestination=null;
		projectMasterDestination=new ProjectMasterDestination();
		BeanUtils.copyProperties(projectMasterSource, projectMasterDestination);
		return projectMasterDestination;
	}

	

	

	

}
