package com.xoriant.bsg.sas.schedulerpoc.db.master.service;

import com.xoriant.bsg.sas.schedulerpoc.request.SourceDatabaseDetails;

public interface SourceBatchService {
	public SourceDatabaseDetails getsouDatabaseDetailsBySchemName(String sourceSchemName);
}
