package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.item.file.FlatFileParseException;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;

public class ProjectMasterCsvIteamReadListener implements ItemReadListener<ProjectMasterCsv> {
	private static Logger LOG = LogManager.getLogger(ProjectMasterCsvIteamReadListener.class.getClass());
	int countRecord=0;
	
	@Override
	public void beforeRead() {
		// TODO Auto-generated method stub
		LOG.info("*************************CountRecord"+countRecord);
	}

	@Override
	public void afterRead(ProjectMasterCsv item) {
		// TODO Auto-generated method stub
		countRecord++;
		LOG.info("CountRecord"+countRecord);

	}

	@Override
	public void onReadError(Exception ex) {
		if (ex instanceof FlatFileParseException) {
			FlatFileParseException flatFileParseException = (FlatFileParseException) ex;
			String message = flatFileParseException.getMessage() + "-" + flatFileParseException.getLineNumber();

			LOG.info("flatFileParseException message" + message);
		}
	}

}
