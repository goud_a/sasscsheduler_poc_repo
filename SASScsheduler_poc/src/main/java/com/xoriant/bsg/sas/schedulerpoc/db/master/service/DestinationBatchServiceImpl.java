package com.xoriant.bsg.sas.schedulerpoc.db.master.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.DestinationBatchDatabase;
import com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository.DestinationBatchDatabaseRepository;
import com.xoriant.bsg.sas.schedulerpoc.request.DestinationDatasourceDetails;

@Service
public class DestinationBatchServiceImpl implements DestinationBatchService {
	@Autowired
	private DestinationBatchDatabaseRepository destinationBatchDatabaseRepository;

	public DestinationDatasourceDetails findBySchemName(String schemName) {
		DestinationDatasourceDetails destinationDatasourceDetails = null;
		DestinationBatchDatabase destinationBatchDatabase = null;
		destinationDatasourceDetails = new DestinationDatasourceDetails();
		destinationBatchDatabase = destinationBatchDatabaseRepository.findByDestinationSchemName(schemName);
		BeanUtils.copyProperties(destinationBatchDatabase, destinationDatasourceDetails);
		return destinationDatasourceDetails;
	}
}
