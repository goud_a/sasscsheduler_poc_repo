package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.schudulers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * This class for scheduling job based on our requirement
 * 
 * @author goud_a
 *
 */
@Component
@EnableScheduling
public class SasEmployeeExperienceDBScheduler {

	@Autowired
	@Qualifier("sasEmployeeJob")
	private Job sasEmployeeJob;

	@Autowired
	private JobLauncher jobLauncher;

	private static Logger LOG = LogManager.getLogger(SasEmployeeExperienceDBScheduler.class.getClass());

	/**
	 * This method configured with cron expression,so it will execute based on
	 * expression
	 * 
	 * @throws JobExecutionAlreadyRunningException
	 * @throws JobRestartException
	 * @throws JobInstanceAlreadyCompleteException
	 * @throws JobParametersInvalidException
	 */

	@Scheduled(cron = "${sas_employee_experience_db_cron_expression__every_minute}")
	public void sasEmployeeJobScheduler() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		JobParameters jobParameters = null;		
		LOG.info("SasEmployeeExperienceDBScheduler starting with:{}", jobParameters);
		jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
		LOG.debug("SasEmployeeExperienceDBScheduler starting with:{}", jobParameters);
		LOG.info("JobLuncher start with JobName: {} and JobParameters: {} ", sasEmployeeJob, jobParameters);
		jobLauncher.run(sasEmployeeJob, jobParameters);
		LOG.debug("JobLuncher start with JobName: {} and JobParameters: {} ", sasEmployeeJob, jobParameters);
	}
}
