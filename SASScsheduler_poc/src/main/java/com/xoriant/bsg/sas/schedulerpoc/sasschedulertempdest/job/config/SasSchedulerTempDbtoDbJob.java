package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.job.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterListener;

//@Configuration
@EnableScheduling
public class SasSchedulerTempDbtoDbJob {
	@Autowired
	@Qualifier("projectMasterDbStep")
	private Step projectMasterDbStep;
	
	@Bean
	public JobExecutionListener projectMasterListener() {
		return new ProjectMasterListener();
	}
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	// 7.job
	@Bean(name = "sasSchedulerTempDbJob")
	public Job sasSchedulerTempDbJob() {
		return jobBuilderFactory.get("sasSchedulerTempDbJob").incrementer(new RunIdIncrementer()).listener(projectMasterListener())
				.start(projectMasterDbStep)
				.listener(projectMasterListener())
				.build();

	}
}
