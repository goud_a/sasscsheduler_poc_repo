package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.batch.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xoriant.bsg.sas.schedulerpoc.db.sasemployee.source.model.UserDetails;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model.SasEmployeeDestinationUserDetails;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.listener.SasEmployeeExperienceStepExecutionListener;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.listener.SasEmployeeJobExecutionListener;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.processor.SasEmployeeUserDetailsProcessor;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.reader.SasEmployeeExperienceReader;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.reader.SasEmployeeUserDetailsReader;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.writer.SasEmployeeUserDetailsWritter;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeesource.source.model.SasEmployeeSourceExperiance;

@Configuration
@EnableScheduling
public class SasEmployeeDBBatchConfig {

	// steps Configurations

// 1.sasEmployeeExperienceStep

	@Autowired
	@Qualifier("sasEmployeeSourceDatasource")
	private DataSource sasEmployeeSourceDatasource;
	
	// Reader
	@Bean
	public ItemReader<SasEmployeeSourceExperiance> sasEmployeeExperienceReader() {
		return new SasEmployeeExperienceReader(sasEmployeeSourceDatasource);
	}
	
	// processor
	@Autowired
	@Qualifier("sasEmployeeExperienceProcessor")
	private ItemProcessor<?, ?> sasEmployeeExperienceItemProcessor;

	// writer

	@Autowired
	@Qualifier("sasEmployeeExperienceWriter")
	private ItemWriter<?> sasEmployeeExperienceWritItemWriter;

	
	// stepExecutionListener
	@Bean
	public StepExecutionListener sasEmployeeExperienceStepExecutionListener() {
		return new SasEmployeeExperienceStepExecutionListener();

	}

	// stepbuilder for create step

	@Autowired
	private StepBuilderFactory sasEmployeeExperienceStepBuilderFactory; 
	
	// 6.step
	@SuppressWarnings("unchecked")
	@Bean
	public Step sasEmployeeExperienceStep() {
		return sasEmployeeExperienceStepBuilderFactory.get("sasEmployeeStep")
				.<SasEmployeeSourceExperiance, SasEmployeeSourceExperiance>chunk(10)
				.reader(sasEmployeeExperienceReader())
				.processor(
						(ItemProcessor<? super SasEmployeeSourceExperiance, ? extends SasEmployeeSourceExperiance>) sasEmployeeExperienceItemProcessor)
				.writer((ItemWriter<? super SasEmployeeSourceExperiance>) sasEmployeeExperienceWritItemWriter)
				.listener(sasEmployeeExperienceStepExecutionListener())

				.build();

	}

// 2.sasEmployeeUserDetailsStep

	// Reader

	@Bean
	public ItemReader<UserDetails> sasEmployeeUserDetailsItemReader() {
		return new SasEmployeeUserDetailsReader(sasEmployeeSourceDatasource);
	}
	
	// processor
	@Autowired
	@Qualifier("sasEmployeeUserDetailsProcessor")
	public ItemProcessor<?,? > sasEmployeeUserDetailsItemProcessor;

	// writer
	@Bean
	public ItemWriter<?> sasEmployeeUserDetailsItemWriter() {
		return new SasEmployeeUserDetailsWritter();
	}

	// stepbuilder for create step
	@Autowired
	private StepBuilderFactory sasEmployeeUserDetailsStepBuilderFactory; 
	
	// step
	@SuppressWarnings("unchecked")
	@Bean
	public Step sasEmployeeUserDetailsStep() {
		return sasEmployeeUserDetailsStepBuilderFactory.get("sasEmployeeUserDetailsStep")
				.<UserDetails, UserDetails>chunk(10).reader(sasEmployeeUserDetailsItemReader())
				.processor((ItemProcessor<? super UserDetails, ? extends UserDetails>) sasEmployeeUserDetailsItemProcessor)
				.writer((ItemWriter<? super UserDetails>) sasEmployeeUserDetailsItemWriter())
				.listener(sasEmployeeExperienceStepExecutionListener())

				.build();

	}

	/**
	 * This method for {@link JobExecutionListener} bean def ,it will return all steps execution and start of job and end of job and etc 
	 * @return
	 */	
	@Bean
	public JobExecutionListener sasEmployeeJobExecutionListener() {
		return new SasEmployeeJobExecutionListener();
	}

	// jobBuilderFactory for Create Job 
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	/**
	 * This method for creating  {@link Job} bean def, it will take care of executing the steps
	 * @return
	 */
	@Bean
	public Job sasEmployeeJob() {
		return jobBuilderFactory.get("sasEmployeeJob")
				.start(sasEmployeeExperienceStep())
				.next(sasEmployeeUserDetailsStep())
				.listener(sasEmployeeJobExecutionListener())
				.incrementer(new RunIdIncrementer())
				.build();

	}
}
