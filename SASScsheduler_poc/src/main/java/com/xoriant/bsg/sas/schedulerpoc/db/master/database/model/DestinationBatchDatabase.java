package com.xoriant.bsg.sas.schedulerpoc.db.master.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "batches_destination_db")
public class DestinationBatchDatabase implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "destination_id")
	private int destinationId;
	@Column(name = "destination_jdbc_url")
	private String destinationUrl;
	@Column(name = "destination_username")
	private String destinationUserName;
	@Column(name = "destination_password")
	private String destinationPassword;
	@Column(name = "destination_driver_class")
	private String destinationDriverClass;
	@Column(name = "destination_schem_name")
	private String destinationSchemName;
	
	
	public String getDestinationSchemName() {
		return destinationSchemName;
	}
	public void setDestinationSchemName(String destinationSchemName) {
		this.destinationSchemName = destinationSchemName;
	}
	public int getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}
	public String getDestinationUrl() {
		return destinationUrl;
	}
	public void setDestinationUrl(String destinationUrl) {
		this.destinationUrl = destinationUrl;
	}
	public String getDestinationUserName() {
		return destinationUserName;
	}
	public void setDestinationUserName(String destinationUserName) {
		this.destinationUserName = destinationUserName;
	}
	public String getDestinationPassword() {
		return destinationPassword;
	}
	public void setDestinationPassword(String destinationPassword) {
		this.destinationPassword = destinationPassword;
	}
	
	public String getDestinationDriverClass() {
		return destinationDriverClass;
	}
	public void setDestinationDriverClass(String destinationDriverClass) {
		this.destinationDriverClass = destinationDriverClass;
	}
	@Override
	public String toString() {
		return "DestinationBatchDatabase [destinationId=" + destinationId + ", destinationUrl=" + destinationUrl
				+ ", destinationUserName=" + destinationUserName + ", destinationPassword=" + destinationPassword
				+ ", destinationDriverClass=" + destinationDriverClass + ", destinationSchemName="
				+ destinationSchemName + "]";
	}
	
	
	

}
