package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.step.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.listener.SasEmployeeExperienceStepExecutionListener;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.reader.SasEmployeeExperienceReader;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeesource.source.model.SasEmployeeSourceExperiance;

//@Configuration
public class EmployeeExperienceDbToDbStep {
	//steps Configurations
	 
		
		// 1.sasEmployeeExperienceStep
		
		@Autowired
		@Qualifier("sasEmployeeSourceDatasource")
		private DataSource sasEmployeeSourceDatasource; 
		// 1.Reader

		@Bean
		public ItemReader<SasEmployeeSourceExperiance> sasEmployeeExperienceReader() {
			return new SasEmployeeExperienceReader(sasEmployeeSourceDatasource);
		} 
		// 2.processor

		@Autowired
		@Qualifier("sasEmployeeExperienceProcessor")
		private ItemProcessor<?, ?> sasEmployeeExperienceItemProcessor;

		// 3.writer

		@Autowired
		@Qualifier("sasEmployeeExperienceWriter")
		private ItemWriter<?> sasEmployeeExperienceWritItemWriter;
		
		@Bean
		public StepExecutionListener sasEmployeeExperienceStepExecutionListener() {
			return new SasEmployeeExperienceStepExecutionListener();
			
		}		
}
