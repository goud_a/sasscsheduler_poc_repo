package com.xoriant.bsg.sas.schedulerpoc.sasschedulertemp.source.model;

public class ProjectMasterSource {
	private int projectId;
	private String projectNumber;
	private String projectName;
	private String projectType;
	private String projectStartDate;
	private String projectEndDate;
	
	          
	public int getProjectId() {
		return projectId;
	}


	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}


	public String getProjectNumber() {
		return projectNumber;
	}


	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getProjectType() {
		return projectType;
	}


	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}


	public String getProjectStartDate() {
		return projectStartDate;
	}


	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}


	public String getProjectEndDate() {
		return projectEndDate;
	}


	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}


	@Override
	public String toString() {
		return "ProjectMaster [projectId=" + projectId + ", projectNumber=" + projectNumber + ", projectName="
				+ projectName + ", projectType=" + projectType + ", projectStartDate=" + projectStartDate
				+ ", projectEndDate=" + projectEndDate + "]";
	}
	
}
