package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;

import com.xoriant.bsg.sas.schedulerpoc.db.master.service.BatchJobAuditService;
import com.xoriant.bsg.sas.schedulerpoc.request.BatchJobAuditRequset;

/**
 * This listener component for to truck the job execution status and we can
 * Capture startTime and EndTime etc
 * 
 * @author goud_a
 *
 */
public class ProjectMasterCsvJobListener implements JobExecutionListener {
	@Autowired
	private BatchJobAuditService batchJobAuditService;
	
	private static final Logger LOG=LogManager.getLogger(ProjectMasterCsvSkipListener.class.getClass());
	@Override
	public void beforeJob(JobExecution jobExecution) {
		
		LOG.info("ON STARTUP => " + jobExecution.getStatus());		
		LOG.info("ON STARTUP => " + jobExecution.getStartTime());
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		// here we can write mail notification logic either failure or success
		// populate the audit date
		// DateFormat dateFormat = null;
		// dateFormat = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
		BatchJobAuditRequset batchJobAuditRequset= null;
		batchJobAuditRequset = new BatchJobAuditRequset();
		batchJobAuditRequset.setSourceName(new ClassPathResource("projectMaster.csv").toString());
		batchJobAuditRequset.setDestinationName("project_master_csv_stg");
		batchJobAuditRequset.setJobName(	jobExecution.getJobInstance().getJobName());
		batchJobAuditRequset.setStartTime(jobExecution.getStartTime());
		batchJobAuditRequset.setEndTime(jobExecution.getEndTime());
		batchJobAuditRequset.setStatus(jobExecution.getStatus().name());
		batchJobAuditRequset.setFauilerMessage(jobExecution.getExitStatus().getExitDescription());
		batchJobAuditRequset.setLastUpdated(jobExecution.getLastUpdated());
		// call the Batch audit
		batchJobAuditService.aduit(batchJobAuditRequset);	
		LOG.info("ON END => " + jobExecution.getStatus());		
		LOG.info("ON END => " + jobExecution.getEndTime());

	}

}
