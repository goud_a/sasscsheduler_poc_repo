package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.repository;

import org.springframework.data.repository.CrudRepository;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterDestination;
/**
 * This repository for persist data into destination database
 * @author goud_a
 *
 */

public interface ProjectMasterDestinationRepo extends CrudRepository<ProjectMasterDestination, Integer> {
	/*
	 * @Query("SELECT * FROM project_master") List<ProjectMaster>
	 * getProjectMasters();
	 */
}
