package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.step.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertemp.source.model.ProjectMasterSource;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.reader.ProjectMasterReader;
//@Configuration
public class ProjectMasterDbToDbStep {
	@Autowired
	@Qualifier("sasSchedulerTempDataSource")
	private DataSource dataSource;

	// 1.Reader
	@Bean
	public ItemReader<ProjectMasterSource> projectMasterReader() {
		return new ProjectMasterReader(dataSource);
	}

	// 2.processor
	@Autowired
	@Qualifier("projectMasterProcessor")
	private ItemProcessor<?, ?> projectMasterProcessor;
	/*
	 * @Bean public ItemProcessor<ProjectMasterSource, ProjectMasterDestination>
	 * projectItemProcessor() { return new ProjectMasterProcessor(); }
	 */
	// 3.Writer
	@Autowired
	@Qualifier("projectmasterWriter")
	private ItemWriter<?> projectmasterWriter;
	// 5.stepBuilderFactory
		@Autowired
		private StepBuilderFactory stepBuilderFactory;

		// 5. step
		@SuppressWarnings("unchecked")
		@Bean(name = "projectMasterDbStep")
		public Step projectMasterDbStep() {
			return this.stepBuilderFactory.get("projectMasterStep").<ProjectMasterSource, ProjectMasterSource>chunk(4)
					.reader(projectMasterReader())
					// .processor(projectItemProcessor())
					.processor(
							(ItemProcessor<? super ProjectMasterSource, ? extends ProjectMasterSource>) projectMasterProcessor)
					.writer((ItemWriter<? super ProjectMasterSource>) projectmasterWriter)
					
					.build();

		}
}
