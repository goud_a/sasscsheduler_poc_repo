package com.xoriant.bsg.sas.schedulerpoc.request;

import javax.persistence.Column;

public class SourceDatabaseDetails {
	private String sourceUrl;
	private String sourceUserName;
	private String sourcePassword;
	private String sourceDriverClass;
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	public String getSourceUserName() {
		return sourceUserName;
	}
	public void setSourceUserName(String sourceUserName) {
		this.sourceUserName = sourceUserName;
	}
	public String getSourcePassword() {
		return sourcePassword;
	}
	public void setSourcePassword(String sourcePassword) {
		this.sourcePassword = sourcePassword;
	}
	public String getSourceDriverClass() {
		return sourceDriverClass;
	}
	public void setSourceDriverClass(String sourceDriverClass) {
		this.sourceDriverClass = sourceDriverClass;
	}
	@Override
	public String toString() {
		return "SourceDatabaseDetails [sourceUrl=" + sourceUrl + ", sourceUserName=" + sourceUserName
				+ ", sourcePassword=" + sourcePassword + ", sourceDriverClass=" + sourceDriverClass + "]";
	}
	
	
	
}
