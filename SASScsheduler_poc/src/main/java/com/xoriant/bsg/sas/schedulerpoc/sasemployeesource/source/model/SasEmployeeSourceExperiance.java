package com.xoriant.bsg.sas.schedulerpoc.sasemployeesource.source.model;

import java.util.Date;

public class SasEmployeeSourceExperiance {
	private int empCode;
	private String client;
	private Date dateOfJoin;
	private String department;
	private String designation;
	private String name;
	private String grade;
	private	String location;
	private double previousExperience;
	private String primarySkills;
	private String project;
	private String reportingManager;
	private String secondarySkills;
	private double totalExperience;
	private double xoriantExperience;
	
	
	public int getEmpCode() {
		return empCode;
	}
	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public Date getDateOfJoin() {
		return dateOfJoin;
	}
	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getPreviousExperience() {
		return previousExperience;
	}
	public void setPreviousExperience(double previousExperience) {
		this.previousExperience = previousExperience;
	}
	public String getPrimarySkills() {
		return primarySkills;
	}
	public void setPrimarySkills(String primarySkills) {
		this.primarySkills = primarySkills;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getReportingManager() {
		return reportingManager;
	}
	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}
	public String getSecondarySkills() {
		return secondarySkills;
	}
	public void setSecondarySkills(String secondarySkills) {
		this.secondarySkills = secondarySkills;
	}
	public double getTotalExperience() {
		return totalExperience;
	}
	public void setTotalExperience(double totalExperience) {
		this.totalExperience = totalExperience;
	}
	public double getXoriantExperience() {
		return xoriantExperience;
	}
	public void setXoriantExperience(double xoriantExperience) {
		this.xoriantExperience = xoriantExperience;
	}
	@Override
	public String toString() {
		return "SasEmployeeSourceExperiance [empCode=" + empCode + ", client=" + client + ", dateOfJoin=" + dateOfJoin
				+ ", department=" + department + ", designation=" + designation + ", name=" + name + ", grade=" + grade
				+ ", location=" + location + ", previousExperience=" + previousExperience + ", primarySkills="
				+ primarySkills + ", project=" + project + ", reportingManager=" + reportingManager
				+ ", secondarySkills=" + secondarySkills + ", totalExperience=" + totalExperience
				+ ", xoriantExperience=" + xoriantExperience + "]";
	}
	
	
	
}
