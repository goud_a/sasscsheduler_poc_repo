package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.listener;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;
import com.xoriant.bsg.sas.schedulerpoc.db.master.service.BatchJobAuditService;
import com.xoriant.bsg.sas.schedulerpoc.request.BatchJobAuditRequset;

public class SasEmployeeJobExecutionListener implements JobExecutionListener {

	private static Logger LOG = LogManager.getLogger(SasEmployeeJobExecutionListener.class.getClass());
	@Autowired
	private BatchJobAuditService batchJobAuditService;
	@Override
	public void beforeJob(JobExecution jobExecution) {
		LOG.info("ON STARTUP => " + jobExecution.getStatus());
		LOG.info("ON STARTUP => " + jobExecution.getStartTime());
	}

	@Override
	public void afterJob(JobExecution jobExecution) {	
		BatchJobAuditRequset batchJobAuditRequset = null;
		batchJobAuditRequset = new BatchJobAuditRequset();
		batchJobAuditRequset.setJobName(jobExecution.getJobInstance().getJobName());
		batchJobAuditRequset.setSourceName("sas_employee_source");
		batchJobAuditRequset.setDestinationName("sas_employee_destination");
		batchJobAuditRequset.setStartTime(jobExecution.getStartTime());
		batchJobAuditRequset.setEndTime(jobExecution.getEndTime());
		batchJobAuditRequset.setLastUpdated(jobExecution.getLastUpdated());
		List<Throwable> allFailureExceptions = jobExecution.getAllFailureExceptions();
		for (Throwable throwable : allFailureExceptions) {

			if (throwable instanceof MysqlDataTruncation) {
				LOG.error(throwable.getMessage());
			}
		}		
		batchJobAuditRequset.setFauilerMessage(jobExecution.getFailureExceptions().toString());
		batchJobAuditRequset.setStatus(jobExecution.getStatus().getBatchStatus().name());
		batchJobAuditService.aduit(batchJobAuditRequset);
		LOG.info("ON END => " + jobExecution.getStatus());
		LOG.info("ON END => " + jobExecution.getEndTime());
	}

}
