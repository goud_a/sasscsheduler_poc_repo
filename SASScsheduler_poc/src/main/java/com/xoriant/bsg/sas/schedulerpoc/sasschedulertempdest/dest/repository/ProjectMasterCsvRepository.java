package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;
/**
 * This repository for persist data into destination database
 * @author goud_a
 *
 */
//@Repository
public interface ProjectMasterCsvRepository extends CrudRepository<ProjectMasterCsv, Integer>{

}
