package com.xoriant.bsg.sas.schedulerpoc.db.master.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.BatchJobAudit;
import com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository.BatchJobAuditRepo;
import com.xoriant.bsg.sas.schedulerpoc.request.BatchJobAuditRequset;
/**
 * This is class for process the data for auditing 
 * @author goud_a
 *
 */
@Service
public class BatchJobAuditServiceImpl implements BatchJobAuditService {
	@Autowired
	private BatchJobAuditRepo batchJobAuditRepo;
	/**
	 * This method for processing the data for auditing 
	 * @param batchAduitRequset
	 */
	@Override
	public void aduit(BatchJobAuditRequset batchAduitRequset) {
		BatchJobAudit batchJobAudit=null;
		batchJobAudit=new BatchJobAudit();
		BeanUtils.copyProperties(batchAduitRequset, batchJobAudit);
		batchJobAuditRepo.save(batchJobAudit);
	}
}
