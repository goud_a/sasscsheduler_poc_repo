package com.xoriant.bsg.sas.schedulerpoc.request;

import javax.persistence.Column;

public class DestinationDatasourceDetails {

	private int destinationId;
	private String destinationUrl;
	private String destinationUserName;
	private String destinationPassword;
	private String destinationDriverClass;
	private String destinationSchemName;
	public int getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(int destinationId) {
		this.destinationId = destinationId;
	}
	public String getDestinationUrl() {
		return destinationUrl;
	}
	public void setDestinationUrl(String destinationUrl) {
		this.destinationUrl = destinationUrl;
	}
	public String getDestinationUserName() {
		return destinationUserName;
	}
	public void setDestinationUserName(String destinationUserName) {
		this.destinationUserName = destinationUserName;
	}
	public String getDestinationPassword() {
		return destinationPassword;
	}
	public void setDestinationPassword(String destinationPassword) {
		this.destinationPassword = destinationPassword;
	}
	public String getDestinationDriverClass() {
		return destinationDriverClass;
	}
	public void setDestinationDriverClass(String destinationDriverClass) {
		this.destinationDriverClass = destinationDriverClass;
	}
	public String getDestinationSchemName() {
		return destinationSchemName;
	}
	public void setDestinationSchemName(String destinationSchemName) {
		this.destinationSchemName = destinationSchemName;
	}
	@Override
	public String toString() {
		return "DestinationDatasourceDetails [destinationId=" + destinationId + ", destinationUrl=" + destinationUrl
				+ ", destinationUserName=" + destinationUserName + ", destinationPassword=" + destinationPassword
				+ ", destinationDriverClass=" + destinationDriverClass + ", destinationSchemName="
				+ destinationSchemName + "]";
	}
	
	
}
