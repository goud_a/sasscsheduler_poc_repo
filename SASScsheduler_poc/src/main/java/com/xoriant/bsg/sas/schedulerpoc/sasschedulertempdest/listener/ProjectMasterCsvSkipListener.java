package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener;

import org.springframework.batch.core.SkipListener;
import org.springframework.batch.item.file.FlatFileParseException;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;

/**
 * This class to see the skipped lines of csv file while reading
 * 
 * @author goud_a
 *
 */
public class ProjectMasterCsvSkipListener implements SkipListener<ProjectMasterCsv, ProjectMasterCsv> {

	@Override
	public void onSkipInRead(Throwable t) {
		if (t instanceof FlatFileParseException) {
			FlatFileParseException flatFileParseException = (FlatFileParseException) t;
			String message = flatFileParseException.getMessage() + "-" + flatFileParseException.getLineNumber();
			System.out.println("*****flatFileParseException message" + message);
		}

	}

	@Override
	public void onSkipInWrite(ProjectMasterCsv item, Throwable t) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSkipInProcess(ProjectMasterCsv item, Throwable t) {
		// TODO Auto-generated method stub

	}

}
