package com.xoriant.bsg.sas.schedulerpoc.config;

import java.util.List;

import org.springframework.boot.env.PropertiesPropertySourceLoader;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;

/**
 * This class for load the custom property file/new property file into
 * {@link Environment} object so we can use those property values anywhere in
 * application using "${keyName}"
 * 
 * @author goud_a
 *
 */
public class CronExpressionsApplicationContextListener
		implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	/**
	 * Initialize the given application context.
	 * @param applicationContext the application to configure
	 */
	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		ConfigurableEnvironment configurableEnvironment = null;
		PropertiesPropertySourceLoader propertiesPropertySourceLoader = null;
		List<PropertySource<?>> propertySources = null;

		propertiesPropertySourceLoader = new PropertiesPropertySourceLoader();
		try {
			propertySources = propertiesPropertySourceLoader.load("config",
					applicationContext.getResource("classpath:cron_expressions.properties"));
			for (PropertySource<?> propertySource : propertySources) {
				configurableEnvironment = applicationContext.getEnvironment();
				configurableEnvironment.getPropertySources().addLast(propertySource);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
