package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.schudulers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * This class for scheduling job based on our requirement
 * 
 * @author goud_a
 *
 */
@Component
@EnableScheduling
public class ProjectMasterCsvScheduler {

	@Autowired
	@Qualifier("projectCsvMasterJob")
	private Job projectMasterCsvJob;
	@Autowired
	private JobLauncher projectMasterCsvJobLauncher;

	private static Logger LOG = LogManager.getLogger(ProjectMasterDBScheduler.class.getClass());

	/**
	 * This method configured with cron expression,so it will execute based on
	 * expression.
	 * 
	 * @throws Exception
	 */
	@Scheduled(cron = "${project_master_csv_cron_epression_every_minute}")
	public void projectMasterCsvScheduler() throws Exception {
		JobParameters jobParameters = null;
		LOG.info("ProjectMasterDBScheduler starting:{}", jobParameters);
		jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
		LOG.debug("ProjectMasterDBScheduler starting:{}", jobParameters);
		projectMasterCsvJobLauncher.run(projectMasterCsvJob, jobParameters);
	}
}
