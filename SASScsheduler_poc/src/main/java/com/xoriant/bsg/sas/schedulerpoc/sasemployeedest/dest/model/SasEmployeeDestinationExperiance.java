package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "experience_stg")
public class SasEmployeeDestinationExperiance {
	@Id
	@Column(name = "emp_code")
	private int empCode;
	private String client;
	@Column(name = "doj")
	private Date dateOfJoin;
	private String department;
	private String designation;
	private String name;	
	private String grade;
	private	String location;
	@Column(name = "previous_exp")
	private double previousExperience;
	@Column(name = "primary_skills")
	private String primarySkills;
	private String project;
	@Column(name = "reporting_manager")
	private String reportingManager;
	@Column(name = "secondary_skills")
	private String secondarySkills;
	@Column(name = "stg_created_on")
	private Date stgCreatedOn;
	@Column(name = "stg_updated_on")
	private Date stgUpdateOn;	
	@Column(name = "total_exp")
	private double totalExperience;
	@Column(name = "xoriant_exp")
	private double xoriantExperience;
	
	
	public int getEmpCode() {
		return empCode;
	}
	public void setEmpCode(int empCode) {
		this.empCode = empCode;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public Date getDateOfJoin() {
		return dateOfJoin;
	}
	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getPreviousExperience() {
		return previousExperience;
	}
	public void setPreviousExperience(double previousExperience) {
		this.previousExperience = previousExperience;
	}
	public String getPrimarySkills() {
		return primarySkills;
	}
	public void setPrimarySkills(String primarySkills) {
		this.primarySkills = primarySkills;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getReportingManager() {
		return reportingManager;
	}
	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}
	public String getSecondarySkills() {
		return secondarySkills;
	}
	public void setSecondarySkills(String secondarySkills) {
		this.secondarySkills = secondarySkills;
	}
	public Date getStgCreatedOn() {
		return stgCreatedOn;
	}
	public void setStgCreatedOn(Date stgCreatedOn) {
		this.stgCreatedOn = stgCreatedOn;
	}
	public Date getStgUpdateOn() {
		return stgUpdateOn;
	}
	public void setStgUpdateOn(Date stgUpdateOn) {
		this.stgUpdateOn = stgUpdateOn;
	}
	public double getTotalExperience() {
		return totalExperience;
	}
	public void setTotalExperience(double totalExperience) {
		this.totalExperience = totalExperience;
	}
	public double getXoriantExperience() {
		return xoriantExperience;
	}
	public void setXoriantExperience(double xoriantExperience) {
		this.xoriantExperience = xoriantExperience;
	}
	@Override
	public String toString() {
		return "SasEmployeeDestinationExperiance [empCode=" + empCode + ", client=" + client + ", dateOfJoin="
				+ dateOfJoin + ", department=" + department + ", designation=" + designation + ", name=" + name
				+ ", grade=" + grade + ", location=" + location + ", previousExperience=" + previousExperience
				+ ", primarySkills=" + primarySkills + ", project=" + project + ", reportingManager=" + reportingManager
				+ ", secondarySkills=" + secondarySkills + ", stgCreatedOn=" + stgCreatedOn + ", stgUpdateOn="
				+ stgUpdateOn + ", totalExperience=" + totalExperience + ", xoriantExperience=" + xoriantExperience
				+ "]";
	}
	
	
}
