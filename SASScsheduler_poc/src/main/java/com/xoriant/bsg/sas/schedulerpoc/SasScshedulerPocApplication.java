package com.xoriant.bsg.sas.schedulerpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xoriant.bsg.sas.schedulerpoc.config.CronExpressionsApplicationContextListener;

@SpringBootApplication
@EnableScheduling
public class SasScshedulerPocApplication {
	
	public static void main(String[] args) {
		SpringApplicationBuilder builder=new SpringApplicationBuilder(SasScshedulerPocApplication.class);
		builder.initializers(new CronExpressionsApplicationContextListener());
		SpringApplication springApplication=builder.build();
		springApplication.run(args);
	}
}
