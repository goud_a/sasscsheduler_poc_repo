package com.xoriant.bsg.sas.schedulerpoc.controller;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * This Rest component for run the job
 * 
 * @author goud_a
 *
 */
//@RestController
@RequestMapping(path = "/batch2")
public class ProjectMasterController {
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	@Qualifier("sasSchedulerTempDbJob")
	private Job sasSchedulerTempDbJob;
	/**
	 * This method for trigger the job and return the status
	 * 
	 * @return status
	 */
	@PostMapping(path = "/start")
	public String projectMasterBatch() {
		JobParameters jobParameters = null;
		jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();

		try {
			JobExecution jobExecution = jobLauncher.run(sasSchedulerTempDbJob, jobParameters);
			return jobExecution.getStatus().toString();
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException exception) {
			exception.printStackTrace();
		}
		return null;
	}
}
