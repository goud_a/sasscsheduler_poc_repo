package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.job.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.listener.SasEmployeeJobExecutionListener;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeesource.source.model.SasEmployeeSourceExperiance;

//@Configuration
@EnableScheduling
public class SasEmployeeDbToDbJob {

	//for one step we need Reader and writer and processor etc
	@Autowired
	@Qualifier("sasEmployeeExperienceReader")
	private ItemReader<?> sasEmployeeExperienceReader;
	
	@Autowired
	@Qualifier("sasEmployeeExperienceProcessor")
	private ItemProcessor<?, ?> sasEmployeeExperienceItemProcessor;
	
	@Autowired
	@Qualifier("sasEmployeeExperienceWriter")
	private ItemWriter<?> sasEmployeeExperienceWritItemWriter;
	
	@Autowired
	private StepBuilderFactory steps;
	
	
	@Bean
	public JobExecutionListener sasEmployeeJobExecutionListener() {
		return new SasEmployeeJobExecutionListener();
	} 
	@SuppressWarnings("unchecked")
	@Bean
	public Step employeeExperienceStep() {
		return steps.get("employeeExperienceStep")
				.<SasEmployeeSourceExperiance, SasEmployeeSourceExperiance>chunk(10)
				.reader((ItemReader<? extends SasEmployeeSourceExperiance>) sasEmployeeExperienceReader)
				.processor(
						(ItemProcessor<? super SasEmployeeSourceExperiance, ? extends SasEmployeeSourceExperiance>) sasEmployeeExperienceItemProcessor)
				.writer((ItemWriter<? super SasEmployeeSourceExperiance>) sasEmployeeExperienceWritItemWriter)
				//.listener(sasEmployeeExperienceStepExecutionListener())				
				.build();
		
	}
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	

	@Bean
	public Job sasEmployeeJob() {
		return jobBuilderFactory.get("sasEmployeeJob").start(employeeExperienceStep())
				.listener(sasEmployeeJobExecutionListener())
				.incrementer(new RunIdIncrementer()).build();

	}
}
