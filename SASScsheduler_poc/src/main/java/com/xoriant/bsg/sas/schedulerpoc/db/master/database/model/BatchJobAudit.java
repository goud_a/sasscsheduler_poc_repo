package com.xoriant.bsg.sas.schedulerpoc.db.master.database.model;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "audit_batch_jobs")
public class BatchJobAudit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aduit_batch_no")
	private Integer auditBatchNo;
	@Column(name = "job_name")
	private String jobName;
	@Column(name = "source_nm")
	private String sourceName;
	@Column(name = "destination_nm")
	private String destinationName;
	@Column(name = "start_time")
	private Date startTime;
	@Column(name = "end_time")
	private Date endTime;
	@Column(name = "status")
	private String status;
	@Column(name = "failure_or_exit_message")
	private String fauilerMessage;
	@Column(name = "last_updated")
	private Date lastUpdated;

	
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Integer getAuditBatchNo() {
		return auditBatchNo;
	}

	public void setAuditBatchNo(Integer auditBatchNo) {
		this.auditBatchNo = auditBatchNo;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFauilerMessage() {
		return fauilerMessage;
	}

	public void setFauilerMessage(String fauilerMessage) {
		this.fauilerMessage = fauilerMessage;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return "BatchJoBAudit [auditBatchNo=" + auditBatchNo + ", sourceName=" + sourceName + ", destinationName="
				+ destinationName + ", startTime=" + startTime + ", endTime=" + endTime + ", status=" + status
				+ ", fauilerMessage=" + fauilerMessage + ", lastUpdated=" + lastUpdated + "]";
	}

}
