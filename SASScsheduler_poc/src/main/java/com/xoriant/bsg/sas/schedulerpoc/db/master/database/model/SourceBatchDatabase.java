package com.xoriant.bsg.sas.schedulerpoc.db.master.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "batches_source_db")
public class SourceBatchDatabase {
	@Id
	@Column(name = "source_id")
	private int sourceId;
	@Column(name = "source_jdbc_url")
	private String sourceUrl;
	@Column(name = "source_username")
	private String sourceUserName;
	@Column(name = "source_password")
	private String sourcePassword;
	@Column(name = "source_driver_class")
	private String sourceDriverClass;
	@Column(name = "source_schem_name")
	private String sourceSchemName;
	
	public String getSourceSchemName() {
		return sourceSchemName;
	}

	public void setSourceSchemName(String sourceSchemName) {
		this.sourceSchemName = sourceSchemName;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getSourceUserName() {
		return sourceUserName;
	}

	public void setSourceUserName(String sourceUserName) {
		this.sourceUserName = sourceUserName;
	}

	public String getSourcePassword() {
		return sourcePassword;
	}

	public void setSourcePassword(String sourcePassword) {
		this.sourcePassword = sourcePassword;
	}

	public String getSourceDriverClass() {
		return sourceDriverClass;
	}

	public void setSourceDriverClass(String sourceDriverClass) {
		this.sourceDriverClass = sourceDriverClass;
	}

	@Override
	public String toString() {
		return "SourceBatchDatabase [sourceId=" + sourceId + ", sourceUrl=" + sourceUrl + ", sourceUserName="
				+ sourceUserName + ", sourcePassword=" + sourcePassword + ", sourceDriverClass=" + sourceDriverClass
				+ ", sourceSchemName=" + sourceSchemName + "]";
	}

	

}
