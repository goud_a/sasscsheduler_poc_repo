package com.xoriant.bsg.sas.schedulerpoc.db.master.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.BatchStepAudit;
import com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository.BatchStepAuditRepo;
import com.xoriant.bsg.sas.schedulerpoc.request.BatchStepRequest;

@Service
public class BatchStepAuditService {
	@Autowired
	private BatchStepAuditRepo batchStepAuditRepo;
	
	
	public void batchStepAudit(BatchStepRequest batchStepRequest) {
		BatchStepAudit batchStepAudit=null;
		batchStepAudit=new BatchStepAudit();
		BeanUtils.copyProperties(batchStepRequest, batchStepAudit);
		batchStepAuditRepo.save(batchStepAudit);
	}
}
