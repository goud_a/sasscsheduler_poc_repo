package com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.BatchJobAudit;
/**
 * This repository for persist the batch audit data
 * @author goud_a
 *
 */
public interface BatchJobAuditRepo extends JpaRepository<BatchJobAudit, Integer> {

}
