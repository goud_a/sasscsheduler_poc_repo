package com.xoriant.bsg.sas.schedulerpoc.request;

import java.util.Date;

import javax.persistence.Column;

public class BatchJobAuditRequset {
	private Integer auditBatchNo;
	private String jobName;
	private String sourceName;
	private String destinationName;
	private Date startTime;
	private Date endTime;
	private int readCount;
	private int readSkipCount;
	private int writeCount;
	private int writeSkipCount;
	private int commitCount;
	private String status;
	private String fauilerMessage;
	private Date lastUpdated;
	
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Integer getAuditBatchNo() {
		return auditBatchNo;
	}
	public void setAuditBatchNo(Integer auditBatchNo) {
		this.auditBatchNo = auditBatchNo;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getReadCount() {
		return readCount;
	}
	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}
	public int getReadSkipCount() {
		return readSkipCount;
	}
	public void setReadSkipCount(int readSkipCount) {
		this.readSkipCount = readSkipCount;
	}
	public int getWriteCount() {
		return writeCount;
	}
	public void setWriteCount(int writeCount) {
		this.writeCount = writeCount;
	}
	public int getWriteSkipCount() {
		return writeSkipCount;
	}
	public void setWriteSkipCount(int writeSkipCount) {
		this.writeSkipCount = writeSkipCount;
	}
	public int getCommitCount() {
		return commitCount;
	}
	public void setCommitCount(int commitCount) {
		this.commitCount = commitCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFauilerMessage() {
		return fauilerMessage;
	}
	public void setFauilerMessage(String fauilerMessage) {
		this.fauilerMessage = fauilerMessage;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
}
