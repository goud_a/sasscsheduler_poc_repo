package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.reader;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.core.RowMapper;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeesource.source.model.SasEmployeeSourceExperiance;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;
/**
 * This class for Reading source table record
 * @author goud_a
 *
 */
public class SasEmployeeExperienceReader extends JdbcCursorItemReader<SasEmployeeSourceExperiance> {

	
	public SasEmployeeExperienceReader(DataSource dataSource) {
		setDataSource(dataSource);
		setSql(ApplicationConstants.SQL_QUERY_FOR_GET_EMPOLYEE_EXPERIENCE);
		setFetchSize(ApplicationConstants.FETCH_SIZE);
		//setRowMapper(new SasEmployeeExperienceRowMapper());
		setRowMapper((rs,rowNum)->{
			SasEmployeeSourceExperiance employeeSourceExperiance=null;
			employeeSourceExperiance=new SasEmployeeSourceExperiance();
			employeeSourceExperiance.setEmpCode(rs.getInt("emp_code"));
			employeeSourceExperiance.setClient(rs.getString("client"));
			employeeSourceExperiance.setDateOfJoin(rs.getDate("doj"));
			employeeSourceExperiance.setDepartment(rs.getString("department"));
			employeeSourceExperiance.setDesignation(rs.getString("designation"));
			employeeSourceExperiance.setName(rs.getString("name"));
			employeeSourceExperiance.setGrade(rs.getString("grade"));
			employeeSourceExperiance.setLocation(rs.getString("location"));
			employeeSourceExperiance.setPreviousExperience(rs.getDouble("previous_exp"));
			employeeSourceExperiance.setPrimarySkills(rs.getString("primary_skills"));
			employeeSourceExperiance.setProject(rs.getString("project"));
			employeeSourceExperiance.setReportingManager(rs.getString("reporting_manager"));
			employeeSourceExperiance.setSecondarySkills(rs.getString("secondary_skills"));
			employeeSourceExperiance.setTotalExperience(rs.getDouble("total_exp"));
			employeeSourceExperiance.setXoriantExperience(rs.getDouble("xoriant_exp"));
			return employeeSourceExperiance;
		});
	}
	/**
	 * This class for {@link RowMapper} implementation we can use this class instead of lambda expression 
	 * @author goud_a
	 *
	 */
	private class SasEmployeeExperienceRowMapper implements RowMapper<SasEmployeeSourceExperiance>{

		@Override
		public SasEmployeeSourceExperiance mapRow(ResultSet rs, int rowNum) throws SQLException {
			SasEmployeeSourceExperiance employeeSourceExperiance=null;
			employeeSourceExperiance=new SasEmployeeSourceExperiance();
			employeeSourceExperiance.setEmpCode(rs.getInt("emp_code"));
			employeeSourceExperiance.setClient(rs.getString("client"));
			employeeSourceExperiance.setDateOfJoin(rs.getDate("doj"));
			employeeSourceExperiance.setDepartment(rs.getString("department"));
			employeeSourceExperiance.setDesignation(rs.getString("designation"));
			employeeSourceExperiance.setName(rs.getString("name"));
			employeeSourceExperiance.setGrade(rs.getString("grade"));
			employeeSourceExperiance.setLocation(rs.getString("location"));
			employeeSourceExperiance.setPreviousExperience(rs.getDouble("previous_exp"));
			employeeSourceExperiance.setPrimarySkills(rs.getString("primary_skills"));
			employeeSourceExperiance.setProject(rs.getString("project"));
			employeeSourceExperiance.setReportingManager(rs.getString("reporting_manager"));
			employeeSourceExperiance.setSecondarySkills(rs.getString("secondary_skills"));
			employeeSourceExperiance.setTotalExperience(rs.getDouble("total_exp"));
			employeeSourceExperiance.setXoriantExperience(rs.getDouble("xoriant_exp"));
			return employeeSourceExperiance;
		}
		
	}
}
