package com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.BatchStepAudit;

public interface BatchStepAuditRepo extends JpaRepository<BatchStepAudit, Integer> {

}
