package com.xoriant.bsg.sas.schedulerpoc.db.master.database.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "audit_batch_jobs_steps")
public class BatchStepAudit {
	@Id
	@Column(name = "step_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stepId;
	@Column(name = "step_name")
	private String stepName;
	@Column(name = "start_time")
	private Date startTime;
	@Column(name = "end_time")
	private Date endtimDate;
	@Column(name = "status")
	private String status;
	@Column(name = "read_count")
	private int readCount;
	@Column(name = "read_skip_count")
	private int readSkipCount;
	@Column(name = "write_count")
	private int writeCount;
	@Column(name = "write_skip_count")
	private int writeSkipCount;
	@Column(name = "commit_count")
	private int commitCount;
	public int getStepId() {
		return stepId;
	}
	public void setStepId(int stepId) {
		this.stepId = stepId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndtimDate() {
		return endtimDate;
	}
	public void setEndtimDate(Date endtimDate) {
		this.endtimDate = endtimDate;
	}
	public int getReadCount() {
		return readCount;
	}
	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}
	public int getReadSkipCount() {
		return readSkipCount;
	}
	public void setReadSkipCount(int readSkipCount) {
		this.readSkipCount = readSkipCount;
	}
	public int getWriteCount() {
		return writeCount;
	}
	public void setWriteCount(int writeCount) {
		this.writeCount = writeCount;
	}
	public int getWriteSkipCount() {
		return writeSkipCount;
	}
	public void setWriteSkipCount(int writeSkipCount) {
		this.writeSkipCount = writeSkipCount;
	}
	public int getCommitCount() {
		return commitCount;
	}
	public void setCommitCount(int commitCount) {
		this.commitCount = commitCount;
	}
	@Override
	public String toString() {
		return "BatchStepAudit [stepId=" + stepId + ", stepName=" + stepName + ", startTime=" + startTime
				+ ", endtimDate=" + endtimDate + ", readCount=" + readCount + ", readSkipCount=" + readSkipCount
				+ ", writeCount=" + writeCount + ", writeSkipCount=" + writeSkipCount + ", commitCount=" + commitCount
				+ "]";
	}
	
	
}
