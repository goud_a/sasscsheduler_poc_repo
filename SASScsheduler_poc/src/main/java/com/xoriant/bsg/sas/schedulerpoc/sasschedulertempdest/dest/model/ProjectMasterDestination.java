package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "project_master_stg")
public class ProjectMasterDestination {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_id")
	private int projectId;
	@Column(name = "project_number")
	private String projectNumber;
	@Column(name = "project_name")
	private String projectName;
	@Column(name = "project_type")
	private String projectType;
	@Column(name = "project_start_date")
	private String projectStartDate;
	@Column(name = "project_end_date")
	private String projectEndDate;
	
	          
	public int getProjectId() {
		return projectId;
	}


	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}


	public String getProjectNumber() {
		return projectNumber;
	}


	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getProjectType() {
		return projectType;
	}


	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}


	public String getProjectStartDate() {
		return projectStartDate;
	}


	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}


	public String getProjectEndDate() {
		return projectEndDate;
	}


	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
}
