package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.db.sasemployee.source.model.UserDetails;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model.SasEmployeeDestinationUserDetails;

/**
 * This class get source table data/record from {@link ItemReader}
 * implementation class and we can perform operation on source table record/data
 * here
 * 
 * @author goud_a
 *
 */
@Component("sasEmployeeUserDetailsProcessor")
public class SasEmployeeUserDetailsProcessor implements ItemProcessor<UserDetails, SasEmployeeDestinationUserDetails> {

	@Override
	public SasEmployeeDestinationUserDetails process(UserDetails item) throws Exception {
		SasEmployeeDestinationUserDetails destinationUserDetails=null;
		destinationUserDetails=new SasEmployeeDestinationUserDetails();
		BeanUtils.copyProperties(item, destinationUserDetails);
		return destinationUserDetails;
	}

}
