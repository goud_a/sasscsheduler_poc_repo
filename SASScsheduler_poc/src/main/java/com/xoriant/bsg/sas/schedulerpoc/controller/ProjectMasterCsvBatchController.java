package com.xoriant.bsg.sas.schedulerpoc.controller;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This Rest component for run the job
 * 
 * @author goud_a
 *
 */
//@RestController
@RequestMapping(path = "/batch1")
public class ProjectMasterCsvBatchController {

	@Autowired
	@Qualifier("sasSchedulerTempCsvJob")
	private Job sasSchedulerTempCsvJob;
	@Autowired
	private JobLauncher jobLauncher;

	/**
	 * This method for trigger the job and return the status
	 * 
	 * @return status
	 */
	@PostMapping(path = "/start")
	public String projectMasterStartBatch() {
		JobParameters jobParameters = null;
		JobExecution jobExecution = null;
		String status = null;
		jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();

		try {
			jobExecution = jobLauncher.run(sasSchedulerTempCsvJob, jobParameters);
			status = jobExecution.getStatus().toString();
			return status;
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException exception) {
			return status;
		}
	}
}
