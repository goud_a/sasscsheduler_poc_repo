package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.writer;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model.SasEmployeeDestinationUserDetails;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.repository.SasEmployeeUserDetailsRepo;

public class SasEmployeeUserDetailsWritter implements ItemWriter<SasEmployeeDestinationUserDetails> {
	@Autowired
	private SasEmployeeUserDetailsRepo sasEmployeeUserDetailsRepo;

	private static final Logger LOG = LogManager.getLogger(SasEmployeeDestinationUserDetails.class.getClass());

	@Override
	public void write(List<? extends SasEmployeeDestinationUserDetails> items) throws Exception {
		String firstName=null;
		for (SasEmployeeDestinationUserDetails sasEmployeeDestinationUserDetails : items) {
			LOG.info("SasEmployeeDestinationUserDetails is persist into destination database :{}",
					sasEmployeeDestinationUserDetails);
			firstName= sasEmployeeUserDetailsRepo.save(sasEmployeeDestinationUserDetails).getFirstName();
			LOG.debug("Sas EmployeeDestinationUserDetails is saved with firstName :{}",firstName);

		}
	}

}
