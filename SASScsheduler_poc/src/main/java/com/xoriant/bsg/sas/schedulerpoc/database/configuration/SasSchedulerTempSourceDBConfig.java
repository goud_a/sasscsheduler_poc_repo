package com.xoriant.bsg.sas.schedulerpoc.database.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.xoriant.bsg.sas.schedulerpoc.db.master.service.SourceBatchService;
import com.xoriant.bsg.sas.schedulerpoc.db.master.service.SourceBatchServiceImpl;
import com.xoriant.bsg.sas.schedulerpoc.request.SourceDatabaseDetails;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;

/**
 * This class for configure the {@link SasSchedulerTempSourceDBConfig}
 * {@link DataSource} by using master schema database
 * 
 * @author goud_a
 *
 */
@Configuration
public class SasSchedulerTempSourceDBConfig implements ApplicationConstants {
	@Autowired
	private SourceBatchService sourceBatchService;

	/**
	 * Below method create the {@link DataSource} object/instance by using
	 * corresponding schema details and fetch schema details using
	 * {@link SourceBatchServiceImpl}
	 * 
	 * @return {@link DataSource}
	 */
	@Bean
	@Primary
	public DataSource sasSchedulerTempDataSource() {
		SourceDatabaseDetails sourceDatabaseDetails = null;
		DriverManagerDataSource driverManagerDataSource = null;
		sourceDatabaseDetails = sourceBatchService.getsouDatabaseDetailsBySchemName(SAS_SCHEDULER_TEMP);
		System.out.println(sourceDatabaseDetails);
		driverManagerDataSource = new DriverManagerDataSource(sourceDatabaseDetails.getSourceUrl(),
				sourceDatabaseDetails.getSourceUserName(), sourceDatabaseDetails.getSourcePassword());
		driverManagerDataSource.setDriverClassName(sourceDatabaseDetails.getSourceDriverClass());
		return driverManagerDataSource;

	}

}