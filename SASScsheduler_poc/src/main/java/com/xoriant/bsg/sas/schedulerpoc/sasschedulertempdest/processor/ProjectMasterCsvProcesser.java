package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.processor;

import org.springframework.batch.item.ItemProcessor;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;


/**
 * This class for to processing the data 
 * @author goud_a
 *
 */
public class ProjectMasterCsvProcesser implements ItemProcessor<ProjectMasterCsv, ProjectMasterCsv> {

	@Override
	public ProjectMasterCsv process(ProjectMasterCsv projectMaster) throws Exception {
		//if want we can process or modify data which ready by org.springframework.batch.item.file.FlatFileItemReader<T>
		return projectMaster;
	}

}
