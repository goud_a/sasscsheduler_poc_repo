package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.repository;

import org.springframework.data.repository.CrudRepository;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model.SasEmployeeDestinationExperiance;

public interface SasEmployeeExperienceRepo extends CrudRepository<SasEmployeeDestinationExperiance, String> {

}
