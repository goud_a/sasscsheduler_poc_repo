package com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.DestinationBatchDatabase;

public interface DestinationBatchDatabaseRepository extends CrudRepository<DestinationBatchDatabase, Integer>{	
	DestinationBatchDatabase findByDestinationSchemName(String destinationSchemName);
}

