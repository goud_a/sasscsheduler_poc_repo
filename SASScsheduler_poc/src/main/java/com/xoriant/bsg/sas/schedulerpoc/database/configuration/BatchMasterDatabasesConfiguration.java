package com.xoriant.bsg.sas.schedulerpoc.database.configuration;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * This class for configure the master database, so we can get the other schema
 * details from master schema
 * 
 * @author goud_a
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository", entityManagerFactoryRef = "batchMasterEntityManagerFactoryBean", transactionManagerRef = "batchMasterTransactionManager"

)
public class BatchMasterDatabasesConfiguration {

	@Autowired
	Environment env;

	/**
	 * This method return the {@link DataSource} object by using master database
	 * credentials
	 * 
	 * @return {@link DataSource}
	 */
	@Bean
	public DataSource batchMasterDatasource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource(
				env.getProperty("spring.datasource.master.url"), env.getProperty("spring.datasource.master.username"),
				env.getProperty("spring.datasource.master.password"));
		dataSource.setDriverClassName(env.getProperty("spring.datasource.master.driver-class-name"));
		return dataSource;
	}

	/**
	 * The below method create JPA {@link EntityManagerFactory} using
	 * {@link LocalContainerEntityManagerFactoryBean} factoryBean component
	 * 
	 * @param dataSource
	 * @return {@link LocalContainerEntityManagerFactoryBean}
	 */
	@Bean
	@Primary
	public LocalContainerEntityManagerFactoryBean batchMasterEntityManagerFactoryBean(
			@Qualifier("batchMasterDatasource") DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = null;
		localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = null;
		hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		localContainerEntityManagerFactoryBean.setDataSource(dataSource);
		localContainerEntityManagerFactoryBean
				.setPackagesToScan("com.xoriant.bsg.sas.schedulerpoc.db.master.database.model");
		hibernateJpaVendorAdapter.setShowSql(true);
		hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5Dialect");
		hibernateJpaVendorAdapter.setGenerateDdl(true);
		localContainerEntityManagerFactoryBean.setJpaVendorAdapter(hibernateJpaVendorAdapter);

		return localContainerEntityManagerFactoryBean;
	}

	/**
	 * The below method create {@link PlatformTransactionManager} by taking
	 * parameter {@link EntityManagerFactory}
	 * 
	 * @param entityManagerFactory
	 * @return {@link PlatformTransactionManager}
	 */
	@Bean
	public PlatformTransactionManager batchMasterTransactionManager(

			@Qualifier("batchMasterEntityManagerFactoryBean") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

}
