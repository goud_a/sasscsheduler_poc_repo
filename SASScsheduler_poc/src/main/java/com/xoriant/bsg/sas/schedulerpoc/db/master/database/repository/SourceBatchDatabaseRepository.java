package com.xoriant.bsg.sas.schedulerpoc.db.master.database.repository;

import org.springframework.data.repository.CrudRepository;

import com.xoriant.bsg.sas.schedulerpoc.db.master.database.model.SourceBatchDatabase;

public interface SourceBatchDatabaseRepository extends CrudRepository<SourceBatchDatabase, Integer>{
	
	SourceBatchDatabase findBySourceSchemName(String sourceSchemName);
}
