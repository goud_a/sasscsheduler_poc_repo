package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;
/**
 * This class for restrict the skip lines while reading the csv file
 * @author goud_a
 *
 */
public class ProjectMasterCsvSkipPolicy implements SkipPolicy {
	/**
	 * This method for restrict the no of lines should ignore while reading if record/row in wrong,
	 * if the maximum limit(what we configured) crosses it will wont process/read further.
	 */
	@Override
	public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
		// TODO Auto-generated method stub
		return (skipCount >=ApplicationConstants.SKIP_LIMIT)?false:true;
	}

}
