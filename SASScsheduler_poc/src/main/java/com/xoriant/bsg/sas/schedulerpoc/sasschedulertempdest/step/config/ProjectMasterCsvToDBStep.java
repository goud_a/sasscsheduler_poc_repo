package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.step.config;

import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvJobListener;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvStepExecutionListener;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.processor.ProjectMasterCsvProcesser;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.reader.ProjectMasterCsvReader;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.writer.ProjectMasterCsvToDatabseWriter;

//@Configuration
public class ProjectMasterCsvToDBStep {
	// 1.Reader
		/**
		 * This bean configuration component read the data from source location here csv
		 * file is source
		 * 
		 * @return
		 */

		@Bean
		public ItemReader<ProjectMasterCsv> prjectItemReader() {
			FlatFileItemReader<ProjectMasterCsv> fileItemReader = new FlatFileItemReader<ProjectMasterCsv>();
			return new ProjectMasterCsvReader(fileItemReader).projectMasterReader();

		}

		// 2.processor
		/**
		 * {@link ItemProcessor} for after read the data by {@link ItemReader} we can
		 * perform any operation
		 * 
		 * @return
		 */
		@Bean
		public ItemProcessor<ProjectMasterCsv, ProjectMasterCsv> projectMasterCsvProcesser() {
			return new ProjectMasterCsvProcesser();

		}

		// 3.writer
		/**
		 * This configured component meant for write the date into destination
		 * 
		 * @return
		 */
		@Bean
		public ItemWriter<ProjectMasterCsv> projectmasterItemWriter() {
			return new ProjectMasterCsvToDatabseWriter();

		}

		/*
		 * // 4.listener //@Bean(name = "projectMasterCsvJobListener")
		 * 
		 * @Bean public JobExecutionListener projectMasterCsvJobListener() { return new
		 * ProjectMasterCsvJobListener(); }
		 */

		// stepbuilderFactory
		@Autowired
		private StepBuilderFactory stepBuilderFactory;

		@Bean
		public StepExecutionListener projectMasterCsvStepExecutionListener() {
			return new ProjectMasterCsvStepExecutionListener();
		}
		// 5.step
		@Bean(name = "projectMasterCsvStep")
		public Step projectMasterCsvStep() {
			return this.stepBuilderFactory.get("projectMasterCsvStep")
					.<ProjectMasterCsv, ProjectMasterCsv>chunk(5)
					.reader(prjectItemReader())
					.processor(projectMasterCsvProcesser())
					.writer(projectmasterItemWriter())
					//.taskExecutor(taskExecutor())
					//.faultTolerant().skipPolicy(projectMasterCsvSkipPolicy())	
					//.listener(projectMasterCsvSkipListener())				
					.listener(projectMasterCsvStepExecutionListener())
					.build();
		}
}
