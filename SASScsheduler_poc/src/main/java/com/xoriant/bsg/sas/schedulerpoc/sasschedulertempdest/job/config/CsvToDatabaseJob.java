package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.job.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener.ProjectMasterCsvJobListener;



//@Configuration
@EnableScheduling
public class CsvToDatabaseJob {
	

	
	@Autowired
	@Qualifier("projectMasterCsvStep")
	private Step projectMasterCsvStep;
	
	@Bean
	public JobExecutionListener projectMasterCsvJobListener() {
		return new ProjectMasterCsvJobListener();
	}
	// jobBuilderFactory
		@Autowired
		private JobBuilderFactory jobBuilderFactory;

		// 5.job
		@Bean(name="sasSchedulerTempCsvJob")
		public Job sasSchedulerTempCsvJob() {
			return jobBuilderFactory.get("sasSchedulerTempCsvJob")
					.incrementer(new RunIdIncrementer())
					.listener(projectMasterCsvJobListener())				
					.start(projectMasterCsvStep)
					.build();
		}

}
