package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.reader;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.handler.SkipLineCallbackCsvHandler;
import com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model.ProjectMasterCsv;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;
/**
 * This class for Read the data from source location file/database
 * @author goud_a
 *
 */
@Component
public class ProjectMasterCsvReader implements ApplicationConstants {	
	
	private FlatFileItemReader<ProjectMasterCsv> reader;
	
	public ProjectMasterCsvReader() {
	}

	public ProjectMasterCsvReader(FlatFileItemReader<ProjectMasterCsv> reader) {
		super();
		this.reader = reader;
	}
	/**
	 * 
	 * @return {@link ItemReader}
	 */
	public ItemReader<ProjectMasterCsv> projectMasterReader() {
		// 1.Read the file from class paths : location
		reader.setResource(new ClassPathResource("projectMaster.csv"));
		//skip the lines of file
		reader.setLinesToSkip(1);
		//To get the skippedLines
		reader.setSkippedLinesCallback(new SkipLineCallbackCsvHandler());
		// 2.Read the data line by line
		reader.setLineMapper(new DefaultLineMapper<ProjectMasterCsv>() {
			{
				// 3.split data based on delimiter lines into Tokenizer
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						//setDelimiter(DELIMITER_COMMA);
						setDelimiter(CSV_FILE_DELIMETTER_PIPE);
						setNames(PROJECT_NUMBER_FIELD_NAME, PROJECT_NAME_FIELD_NAME, PROJECT_TYPE_FIELD_NAME, PROJECT_START_DATE_FIELD_NAME, PROJECT_END_DATE_FIELD_NAME,PROJECT_ID_FIELD_NAME);
					}
				});
				//4.map or create object from tokenizer
				setFieldSetMapper(new BeanWrapperFieldSetMapper<ProjectMasterCsv>() {
					{
						setTargetType(ProjectMasterCsv.class);
					}
				});
			}
		});	 
		return reader;
	}
	
}
