package com.xoriant.bsg.sas.schedulerpoc.utils;

public interface ApplicationConstants {

	

	
	public static final String SAS_EMPLOYEE_DESTINATION = "sas_employee_destination";
	public static final String SAS_EMPLOYEE_SOURCE = "sas_employee_source";


	public static final String SAS_SCHEDULER_TEMP = "sas_scheduler_temp";
	public static final String SAS_SCHEDULER_TEMP_DESTINATION = "sas_scheduler_temp_destination";

	public static final int SKIP_LIMIT = 2;

	// SQL Quaries
	public static final String SQL_QUERY_FOR_GET_PROJECT_MASTER="SELECT project_number,project_name,project_type,project_start_date,project_end_date,project_id FROM project_master";
	public static final String SQL_QUERY_FOR_GET_EMPOLYEE_EXPERIENCE = "SELECT experience.emp_code,experience.client,experience.doj,experience.department,experience.designation,experience.name,experience.grade,experience.location,experience.previous_exp,experience.primary_skills,experience.project,experience.reporting_manager,experience.secondary_skills,experience.stg_created_on,experience.stg_updated_on,experience.total_exp,experience.xoriant_exp FROM sas_employee_source.experience";
	public static final String SQL_VIEW_FOR_GET_USER_DETAILS="SELECT first_name, last_name, mobile_number, street, city, state, country FROM sas_employee_source.user_details_view";
	//sas_employee_source.experience
	public static final int FETCH_SIZE = 100;
	
	
	// SQL Query columns Constraints
	
	public static final String FIRST_NAME_COLUMN_NAME="first_name";
	public static final String LAST_NAME_COLUMN_NAME="last_name";
	public static final String MOBILE_NUMBER_COLUMN_NAME="mobile_number";
	public static final String STREET_COLUMN_NAME="street";
	public static final String CITY_COLUMN_NAME="city";
	public static final String STATE_COLUMN_NAME="state";
	public static final String COUNTRY_COLUMN_NAME="country";
	public static final String PROJECT_NUMBER_COLUMN_NAME="project_number";
	public static final String PROJECT_TYPE_COLUMN_NAME="project_type";
	public static final String PROJECT_NAME_COLUMN_NAME="project_name";
	public static final String PROJECT_START_DATE_COLUMN_NAME="project_start_date";
	public static final String PROJECT_END_DATE_COLUMN_NAME="project_end_date";
	public static final String PROJECT_ID_COLUMN_NAME="project_id";
	
	//Csv file field Constraints
	
	public static final String PROJECT_NUMBER_FIELD_NAME="projectNumber";
	public static final String PROJECT_TYPE_FIELD_NAME="projectType";
	public static final String PROJECT_NAME_FIELD_NAME="projectName";
	public static final String PROJECT_START_DATE_FIELD_NAME="projectStartDate";
	public static final String PROJECT_END_DATE_FIELD_NAME="projectEndDate";
	public static final String PROJECT_ID_FIELD_NAME="projectId";
	
	public static final int USER_INITAL_ID_ZERO=0;
	
	public static final String CSV_FILE_DELIMETTER_PIPE="|";
}
