package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.listener;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;

import com.xoriant.bsg.sas.schedulerpoc.db.master.service.BatchStepAuditService;
import com.xoriant.bsg.sas.schedulerpoc.request.BatchStepRequest;

public class SasEmployeeExperienceStepExecutionListener implements StepExecutionListener {
	
	@Autowired
	private BatchStepAuditService batchStepAuditService;
	
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		//String exceptionMessage=null;
		BatchStepRequest batchStepRequest=null;
		batchStepRequest=new BatchStepRequest();
		batchStepRequest.setStepName(stepExecution.getStepName());
		batchStepRequest.setStartTime(stepExecution.getStartTime());
		batchStepRequest.setEndTime(stepExecution.getEndTime());
		//batchStepRequest.setLastUpdated(stepExecution.getLastUpdated());
		batchStepRequest.setStatus(stepExecution.getStatus().getBatchStatus().toString());
		//batchAuditRequset.setFauilerMessage(stepExecution.getFailureExceptions().toString());

		List<Throwable> failureExceptions = stepExecution.getFailureExceptions();
		for (Throwable throwable : failureExceptions) {
			if (throwable instanceof BadSqlGrammarException) {
				BadSqlGrammarException badSqlGrammarException=(BadSqlGrammarException) throwable;
				//batchAuditRequset.setFauilerMessage(badSqlGrammarException.getMessage());
			}
		}
		
		batchStepRequest.setReadCount(stepExecution.getReadCount());
		batchStepRequest.setReadSkipCount(stepExecution.getReadSkipCount());
		batchStepRequest.setWriteCount(stepExecution.getWriteCount());
		batchStepRequest.setWriteSkipCount(stepExecution.getWriteSkipCount());
		batchStepRequest.setCommitCount(stepExecution.getCommitCount());
		batchStepAuditService.batchStepAudit(batchStepRequest);
		return null;
	}

}
