package com.xoriant.bsg.sas.schedulerpoc.database.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.xoriant.bsg.sas.schedulerpoc.db.master.service.DestinationBatchService;
import com.xoriant.bsg.sas.schedulerpoc.request.DestinationDatasourceDetails;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;
/**
 * This class for configure the Destination Database like {@link DataSource},
 * {@link EntityManagerFactory}, {@link PlatformTransactionManager}
 * 
 * @author goud_a
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
		"com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.repository" }, entityManagerFactoryRef = "destinationEntityManagerFactory", transactionManagerRef = "destinationTransactionManager")
public class SasShedulerTempDestinationDBConfig implements ApplicationConstants {

	@Autowired
	private DestinationBatchService batchService;
	/**
	 * Below method create the {@link DataSource} object/instance by using
	 * corresponding schema details and fetch schema details using
	 * {@link DestinationBatchService}
	 * 
	 * @return {@link DataSource}
	 */
	@Bean
	public DataSource sasShedulerTempDestination() {
		DestinationDatasourceDetails destinationDatasourceDetails = null;
		DriverManagerDataSource dataSource = null;
		destinationDatasourceDetails = batchService
				.findBySchemName(SAS_SCHEDULER_TEMP_DESTINATION);
		dataSource = new DriverManagerDataSource(destinationDatasourceDetails.getDestinationUrl(),
				destinationDatasourceDetails.getDestinationUserName(),
				destinationDatasourceDetails.getDestinationPassword());
		dataSource.setDriverClassName(destinationDatasourceDetails.getDestinationDriverClass());
		dataSource.setSchema(destinationDatasourceDetails.getDestinationSchemName());
		return dataSource;
	}
	/**
	 * The below method create JPA {@link EntityManagerFactory} using
	 * {@link LocalContainerEntityManagerFactoryBean} factoryBean component
	 * 
	 * @param dataSource
	 * @return {@link LocalContainerEntityManagerFactoryBean}
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean destinationEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(sasShedulerTempDestination())
				.packages("com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.dest.model")
				.properties(jpaProperties()).build();
	}

	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		props.put("hibernate.hbm2ddl.auto", "update");
		props.put("hibernate.show_sql", "true");
		return props;
	}
	/**
	 * The below method create {@link PlatformTransactionManager} by taking
	 * parameter {@link EntityManagerFactory}
	 * 
	 * @param entityManagerFactory
	 * @return {@link PlatformTransactionManager}
	 */
	@Bean(name = "destinationTransactionManager")
	public PlatformTransactionManager destinationTransactionManager(
			@Qualifier("destinationEntityManagerFactory") EntityManagerFactory destinationEntityManagerFactory) {
		return new JpaTransactionManager(destinationEntityManagerFactory);
	}
}
