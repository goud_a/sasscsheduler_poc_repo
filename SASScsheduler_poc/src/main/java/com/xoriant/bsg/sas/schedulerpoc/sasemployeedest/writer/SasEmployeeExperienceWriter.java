package com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model.SasEmployeeDestinationExperiance;
import com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.repository.SasEmployeeExperienceRepo;

@Component("sasEmployeeExperienceWriter")
public class SasEmployeeExperienceWriter implements ItemWriter<SasEmployeeDestinationExperiance> {
	@Autowired
	private SasEmployeeExperienceRepo sasEmployeeExperienceRepo;

	@Override
	public void write(List<? extends SasEmployeeDestinationExperiance> items) throws Exception {
		for (SasEmployeeDestinationExperiance sasEmployeeDestinationExperiance : items) {
			sasEmployeeExperienceRepo.save(sasEmployeeDestinationExperiance);
		}
	}

}
