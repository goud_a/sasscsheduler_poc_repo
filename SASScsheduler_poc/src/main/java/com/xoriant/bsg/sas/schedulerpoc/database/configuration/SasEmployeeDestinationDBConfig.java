package com.xoriant.bsg.sas.schedulerpoc.database.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.xoriant.bsg.sas.schedulerpoc.db.master.service.DestinationBatchService;
import com.xoriant.bsg.sas.schedulerpoc.request.DestinationDatasourceDetails;
import com.xoriant.bsg.sas.schedulerpoc.utils.ApplicationConstants;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
		"com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.repository" }, entityManagerFactoryRef = "sasEmployeeDestinationEntityManagerFactory", transactionManagerRef = "sasEmployeeDestinationTransactionManager")
/**
 * This class for configure the Destination Database like {@link DataSource},
 * {@link EntityManagerFactory}, {@link PlatformTransactionManager}
 * 
 * @author goud_a
 *
 */
public class SasEmployeeDestinationDBConfig {

	@Autowired
	private DestinationBatchService batchService;
/**
 * 
 * @return {@link DataSource}
 */
	@Bean
	public DataSource sasEmployeeDestinationDatasource() {
		DestinationDatasourceDetails destinationDatasourceDetails = null;
		DriverManagerDataSource dataSource = null;
		destinationDatasourceDetails = batchService.findBySchemName(ApplicationConstants.SAS_EMPLOYEE_DESTINATION);
		dataSource = new DriverManagerDataSource(destinationDatasourceDetails.getDestinationUrl(),
				destinationDatasourceDetails.getDestinationUserName(),
				destinationDatasourceDetails.getDestinationPassword());
		dataSource.setDriverClassName(destinationDatasourceDetails.getDestinationDriverClass());
		return dataSource;
	}

	/**
	 * This method for create the EntityManager through the FactoryBean class
	 * LocalContainerEntityManagerFactoryBean
	 * 
	 * @param builder
	 * @return
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean sasEmployeeDestinationEntityManagerFactory(
			EntityManagerFactoryBuilder builder) {
		return builder.dataSource(sasEmployeeDestinationDatasource())
				.packages("com.xoriant.bsg.sas.schedulerpoc.sasemployeedest.dest.model")
				.properties(jpaProperties())

				.build();
	}

	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		props.put("hibernate.hbm2ddl.auto", "update");
		props.put("hibernate.show_sql", "true");
		return props;
	}

	/**
	 * This method for Transaction through {@link PlatformTransactionManager}
	 * 
	 * @param entityManagerFactory
	 * @return
	 */
	@Bean
	public PlatformTransactionManager sasEmployeeDestinationTransactionManager(
			@Qualifier("sasEmployeeDestinationEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
