package com.xoriant.bsg.sas.schedulerpoc.sasschedulertempdest.listener;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

public class ProjectMasterCsvStepExecutionListener implements StepExecutionListener {

	private static Logger LOG=LogManager.getLogger(ProjectMasterCsvStepExecutionListener.class.getClass());
	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		int readCount=stepExecution.getReadCount();
		LOG.info("The current number of items read for this execution"+readCount);
		int skipCount=stepExecution.getSkipCount();
		LOG.info("The total number of items skipped"+skipCount);
		int commitCount = stepExecution.getCommitCount();
		LOG.info("The current number of commits"+commitCount);
		JobExecution jobExecution = stepExecution.getJobExecution();
		
		int processSkipCount = stepExecution.getProcessSkipCount();
		LOG.info("The number of records skipped during processing"+processSkipCount);
		
		int writeCount = stepExecution.getWriteCount();
		LOG.info("The current number of items written for this execution"+writeCount);
		int writeSkipCount = stepExecution.getWriteSkipCount();
		LOG.info("The number of records skipped on write"+writeSkipCount);
		List<Throwable> failureExceptions = stepExecution.getFailureExceptions();
		
		return null;
	}

}
