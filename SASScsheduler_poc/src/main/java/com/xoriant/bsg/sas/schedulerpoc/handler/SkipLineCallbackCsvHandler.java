package com.xoriant.bsg.sas.schedulerpoc.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineCallbackHandler;

/**
 * This callbackHanlder to get the skip line While {@link FlatFileItemReader}
 * reading
 * 
 * @author goud_a
 *
 */
public class SkipLineCallbackCsvHandler implements LineCallbackHandler {

	private static Logger LOG = LogManager.getLogger(SkipLineCallbackCsvHandler.class.getClass());

	@Override
	public void handleLine(String line) {
		LOG.info("skipLine:" + line);
	}

}
